'''
Created on Feb 2, 2020
Updated on May 5, 2020
@author: alina.seiceanu
'''
import os
from selenium import webdriver
from selenium.common.exceptions import TimeoutException, NoSuchElementException,\
    InvalidSessionIdException, InvalidArgumentException, NoSuchFrameException
from os import path
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait  
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver import ActionChains
import logging
import createTicketParameters
#from selenium.common.exceptions import NoSuchElementException
from time import sleep
#import insertTicketdetails
from selenium.webdriver.remote.switch_to import SwitchTo
#from clear_cache import clear_firefox_cache
#########################################
##### Fill in credentials ######
#########################################
logging.basicConfig(filename='TicketCreator.log', filemode='a+', format='%(asctime)s - %(levelname)s - %(message)s',level=logging.INFO)
baseUrl="http://ticketing-tool"
user = "your-user"
password = "your-password"
#########################################
driver=webdriver.Firefox()
browser = webdriver.Firefox()
action=ActionChains(browser)
browser.implicitly_wait(120)
timeout = 10
sleepTimer = 10
global itemId
global currentPage
#Wait to load
def waitForHomePage():
        try:
            element_present = EC.presence_of_element_located((By.ID, 'titreConnexionManuelle'))
            WebDriverWait(browser, timeout).until(element_present)
        except TimeoutException:
            logging.error('Timed out waiting for page to load')
            return
def selDropDown(id,value):
    try:
        select = Select(browser.find_element_by_id(id))
        select.select_by_visible_text(value)
    except InvalidElementIdException:
        logging.error('DropDown %s does not exist',id)
        return
def logIn(baseUrl,user,password):
    try:
        browser.get(baseUrl )
        waitForHomePage()
        browser.find_element_by_id('user').send_keys(user)
        browser.find_element_by_id('password').send_keys(password)
        browser.find_element_by_id("spanBtnCnx").click()
        sleep(sleepTimer)
    except InvalidArgumentException:
        return
def switchWindow(handles):
    try:
     size = len(handles)
     parent_handle = browser.current_window_handle
     for x in range(size):
        if handles[x] != parent_handle:
            browser.switch_to.window(handles[x])
            sleep(sleepTimer)
    except NoSuchWindowException:
        return 
def switchFrame(Frame=1):
    try:
        browser.switch_to.frame(Frame)
    except NoSuchFrameException:
        logging.error("cannot switch to frame")
        return
    
from selenium.common.exceptions import NoSuchElementException        
def check_exists_by_xpath(selector):
    try:
        browser.find_element_by_xpath(selector)
    except NoSuchElementException:
        return False
    return True    
def resourceTicket():
    try:
        browser.find_element_by_id("menu_hlnk_I1_L1").click();
        browser.find_element_by_id("menu_hlnk_I1_2_L2").click();
        browser.find_element_by_xpath("//option[@value='2']").click();
    except NoSuchElementException:
        return
def searchPlatformAndCreateTicket(Identifier1):
    browser.find_element_by_name("IDT11").send_keys(Identifier1)
    browser.find_element_by_xpath("//button[3]/span").click()
    sleep(2)
    if  check_exists_by_xpath("//tr[1]/td/div/a[1]/img"):
        browser.find_element_by_xpath("//tr[1]/td/div/a[1]/img").click()
        sleep(sleepTimer)    
                 
    else:
        browser.find_element_by_xpath("//a[@id='createButton']/span").click()
        logging.info("create resource for None identifier")
        print("platform not found, used the identifier"+Identifier1)
        browser.find_element_by_xpath("//select[@id='DropDownDomain']/option[text()='Sites']").click()
        browser.find_element_by_xpath("//select[@id='ResourceType']/option[text()='Sites']").click()
        browser.find_element_by_xpath("//span[contains(.,'save resource and create ticket')]").click()
        logging.info('Created a new Ticket for %s', Identifier1)
        
def createTicket(): 
    global TicketID 
    TicketID = browser.title.split()[2]
    logging.info('%s has been opened',TicketID)
    Origin=TicketParameters.Origin
    Ticket_type=TicketParameters.Ticket_type
    Identifier1=TicketParameters.Identifier1 
    Technical_impact=TicketParameters.Technical_impact
    Processing_priority = TicketParameters.Processing_priority
    Descriere=TicketParameters.Description
    Group_ID =TicketParameters.Group_ID
    #Select from dropdown, fill all ticket fields
    sleep(2)
    switchFrame(6)
    Desc = browser.find_element_by_css_selector(".cke_editable")
    Desc.send_keys(Descriere)
    Desc.send_keys(Keys.TAB)
    sleep(2)
    browser.switch_to.default_content()
    switchFrame(1)
    selDropDown('IDTORIAPP', Origin)
    selDropDown('IDTTYPTIC', Ticket_type)
    selDropDown('IDTNTR', Technical_impact )
    selDropDown('STRIDTPRI', Processing_priority)
    selDropDown('STRIDTPRI', Processing_priority)
    sleep(2)
    browser.find_element_by_id("menu_lbtn_I5_L1").click()
    sleep(sleepTimer)
    #Click on activate
    browser.find_element_by_xpath("//button[5]/span").click()
    #Fill Group ID
    v=browser.find_element_by_xpath("//div[@id='SC088-1']/fieldset[2]/div/div/div/div[2]/input")
    v.send_keys(Group_ID)
    v.send_keys(Keys.ENTER)
    sleep(sleepTimer)
    browser.find_element_by_xpath("//a[contains(text(),'save and exit')]").click()
    logging.info('%s has been created',TicketID)
    browser.close()
    browser.quit()
    createTicketParameters.update_tickets(TicketID,TicketParameters.Alarm_ID)
    createTicketParameters.send_email(TicketID)
    sleep(5)
     
def create():
    logIn(baseUrl,user,password)
    logging.info('successfully logged in')
    switchWindow(browser.window_handles)
    switchFrame(1)
    resourceTicket()
    searchPlatformAndCreateTicket(TicketParameters.Identifier1)
    logging.info('%s Platform used to create new ticket',TicketParameters.Identifier1)
    switchWindow(browser.window_handles)
    sleep(sleepTimer)
    switchFrame(1) 
    createTicket()
    
createTicketParameters.OceaneParameters()
import TicketParameters  
def main(): 
  f=open('TicketParameters.py',"r")
  if 'Identifier1' in f.read():
    create()
    f.close()
    createTicketParameters.renameTheConfig(TicketID)
  else:
    logging.info('No new data found to open ticket')
    browser.close()
    browser.quit()
if __name__ == "__main__":
    main()
    
os.system("taskkill /f /im  firefox.exe")